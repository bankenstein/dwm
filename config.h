/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>
/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "JetBrains Mono Nerd Font:size=14", "Noto Color Emoji:size=14" };
static const char dmenufont[]       = "Eternal UI:size=12";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";

static const char col_yellow[]      = "#FAE100";
static const char col_purple[]      = "#900DFF";
static const char col_turquoise[]   = "#32DBF0";
static const char col_hotpink[]     = "#FF0181";
static const char col_grey[]        = "#272727";
static const char col_black[]       = "#000000";

static const char *colors[][3]      = {
    /*               fg             bg         border   */
    [SchemeNorm] = { col_yellow,    col_black, col_yellow     },
    [SchemeSel]  = { col_turquoise, col_black, col_turquoise  },
};

/* tagging */
static const char *tags[] = { " ", " ", " ", " ", " ", " ", " ", " ", "" };

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */

        // Note that "tags mask" is not "put it on this tag", it's "put these tags on it".
        // That's defined via a binary number - 
        // the tags applied correspond to the positions of the ones.
        // (Yes, you are supposed to use the bit shift operator.)
        // See https://dwm.suckless.org/customisation/tagmask/ for details.
    /* class             instance          title       tags mask   iscentered    isfloating   monitor */
    { "firefox",         NULL,             NULL,       1 << 1,     0,            0,           -1 },
    { "qutebrowser",     NULL,             NULL,       1 << 1,     0,            0,           -1 },
    { "discord",         NULL,             NULL,       1 << 2,     0,            0,           -1 },
    { "TelegramDesktop", NULL,             NULL,       1 << 2,     0,            0,           -1 },
    { "VSCodium",        NULL,             NULL,       1 << 3,     0,            0,           -1 },
    { "krita",           NULL,             NULL,       1 << 4,     0,            0,           -1 },
    { "lmms",            NULL,             NULL,       1 << 4,     0,            0,           -1 },
    { "kdenlive",        NULL,             NULL,       1 << 4,     0,            0,           -1 },
    { "Steam",           NULL,             NULL,       1 << 7,     0,            0,           -1 },
    { "kitty",           "musicplayer",    NULL,       1 << 8,     0,            0,           -1 },
    { "kitty",           "kitty-floating", NULL,       0,          1,            1,           -1 },
    { "ark",             NULL,             NULL,       0,          1,            1,           -1 },
    { "Pavucontrol",     NULL,             NULL,       0,          1,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "﩯",       tile },    /* first entry is default */
    { " ",       NULL },    /* no layout function means floating behavior */
    { " ",       monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define SUPER Mod4Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "j4-dmenu-desktop", NULL};
static const char *termcmd[]  = { "kitty", NULL };
static const char *browsercmd[]  = { "firefox", NULL };
static const char *browsercmd2[]  = { "qutebrowser", NULL };
static const char *emailcmd[]  = { "thunderbird", NULL };
static const char *editorcmd[]  = { "vscodium", NULL };
static const char *musiccmd[]  = { "kitty", "--name", "musicplayer", "ncspot", NULL };
static const char *musiccmd2[]  = { "flatpak", "run", "com.spotify.Client", NULL };
static const char *shutdowncmd[]  = { "shutdown", "now", NULL };
static const char *suspendcmd[]  = { "systemctl", "suspend", NULL };
static const char *screenshotcmd[]  = { "spectacle", "-f", NULL };
static const char *screenshotpartialcmd[]  = { "spectacle", "-r", NULL };
static const char *screenshotguicmd[]  = { "spectacle", "-g", NULL };

static const char *upvol[]     = { "/usr/bin/amixer", "set", "Master", "1%+", NULL };
static const char *downvol[]   = { "/usr/bin/amixer", "set", "Master", "1%-", NULL };
static const char *uplight[]   = { "brightnessctl", "set", "+5%", NULL };
static const char *downlight[] = { "brightnessctl", "set", "5%-", NULL };

static const char *prev[]      = {"playerctl", "previous", NULL };
static const char *next[]      = {"playerctl", "next", NULL };
static const char *playpause[] = {"playerctl", "play-pause", NULL };

// static const char *mutevol[] = { "/usr/bin/amixer", "set", "Master", "toggle", NULL };
// Muting the speakers via Fn+F1 is hardware-based.
// This key combination is reserved specifically for this action.

static Key keys[] = {
    { 0,                            XF86XK_AudioLowerVolume,  spawn, {.v = downvol}   },
    { 0,                            XF86XK_AudioRaiseVolume,  spawn, {.v = upvol}     },
    { 0,                            XF86XK_MonBrightnessDown, spawn, {.v = downlight} },
    { 0,                            XF86XK_MonBrightnessUp,   spawn, {.v = uplight}   },


    { Mod3Mask,                     XK_Down,                  spawn, {.v = downvol}   },
    { Mod3Mask,                     XK_Up,                    spawn, {.v = upvol}     },
    { Mod3Mask,                     XK_Left,                  spawn, {.v = prev}       },
    { Mod3Mask,                     XK_Right,                 spawn, {.v = next}       },
    { Mod3Mask,                     XK_space,                 spawn, {.v = playpause} },


    /* modifier                     key        function        argument */
    { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
    { SUPER,                        XK_a,      spawn,          SHCMD("j4-dmenu-desktop --dmenu='dmenu -i -fn \"Eternal UI:pixelsize=18\"'") },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
    { 0,                            XK_F12,    spawn,          {.v = termcmd } },
    { SUPER,                        XK_b,      spawn,          {.v = browsercmd } },
    { SUPER|ShiftMask,              XK_b,      spawn,          {.v = browsercmd2 } },
    { SUPER,                        XK_e,      spawn,          {.v = emailcmd } },
    { SUPER,                        XK_c,      spawn,          {.v = editorcmd } },
    { SUPER,                        XK_s,      spawn,          {.v = musiccmd } },
    { SUPER|ShiftMask,              XK_s,      spawn,          {.v = musiccmd2 } },
    { MODKEY|ControlMask,           XK_q,      spawn,          {.v = shutdowncmd } },
    { MODKEY|ControlMask,           XK_s,      spawn,          {.v = suspendcmd } },
    { 0,                            XK_Print,  spawn,          {.v = screenshotcmd } },
    { ShiftMask,                    XK_Print,  spawn,          {.v = screenshotpartialcmd } },
    { ControlMask,                  XK_Print,  spawn,          {.v = screenshotguicmd } },
    { MODKEY,                       XK_b,      togglebar,      {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    // Good old Alt+Tab
    { MODKEY,                       XK_Tab,    focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
    { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Return, zoom,           {0} },
    { MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
    { MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
    { MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
    { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
    // Alt + Tab removed as tag switching shortcut due to being a "security vulnerability" in programming classes.
    // { MODKEY,                       XK_Tab,    view,           {0} },
    { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
    { Mod1Mask,                     XK_F4,     killclient,     {0} },
    { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
    { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
    { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
    { MODKEY,                       XK_space,  setlayout,      {0} },
    { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
    { MODKEY,                       XK_Prior,  focusmon,       {.i = -1 } },
    { MODKEY,                       XK_Next,   focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_Prior,  tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,             XK_Next,   tagmon,         {.i = +1 } },
    { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
    { MODKEY,                       XK_comma,  viewprev,       {0} },
    { MODKEY,                       XK_period, viewnext,       {0} },
    { MODKEY|ShiftMask,             XK_comma,  tagtoprev,      {0} },
    { MODKEY|ShiftMask,             XK_period, tagtonext,      {0} },
    TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_9,                      8)
    { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

